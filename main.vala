Gee.HashSet readWords(string filename) {
    File file = File.new_for_path(filename);
    Gee.HashSet<string> words = new Gee.HashSet<string>();

    if (!file.query_exists()) {
        stdout.printf("File `%s` doesn't exist.\n", file.get_path());
    } else {
        try {
            DataInputStream dis = new DataInputStream(file.read());
            string line;
            while ((line = dis.read_line(null)) != null) {
                words.add(line);
            }
        } catch (Error e) {
            stdout.printf("Error while extracting words from file: %s",
                e.message);
        }
    }

    return words;
}

int main(string[] args) {
    // assuming a filename has been passed
    if (args.length == 2) {
        Gee.HashSet<string> words = readWords(args[1]);
        POSDeterminer d = new POSDeterminer(words);
        d.saveWordsToFiles();
    } else {
        stdout.printf(
            "Pass in a filename (only). Words in the file should " +
            "be in a new line each."
        );
    }

    return 0;
}
