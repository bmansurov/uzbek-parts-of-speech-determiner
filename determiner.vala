enum POS {
    NOUN,  // от
    ADJECTIVE,  // сифат
    NUMERAL,  // сон
    PRONOUN,  // олмош
    VERB,  // феъл
    ADVERB,  // равиш
    AUXILIARY,  // кўмакчи
    CONJUNCTION,  // боғловчи
    PARTICLE,  // юклама
    INTERJECTION,  // ундов
    IMITATIVE,  // тақлидий
    MODAL,  // модал
    UNKNOWN,  // номаълум сўзлар
}

public class POSDeterminer : Object {
    // All words in the language
    // key is the word, value is the list of POSes
    private Gee.HashMap<string, Gee.HashSet<int>> dictionary;
    // all known numerals in the language
    private Gee.HashSet<string> numerals;
    // all known pronouns in the language
    private Gee.HashSet<string> pronouns;
    // all known auxiliaries in the language
    private Gee.HashSet<string> auxiliaries;
    // all known conjunctions in the language
    private Gee.HashSet<string> conjunctions;
    // all known particles in the language
    private Gee.HashSet<string> particles;
    // all known interjections in the language
    private Gee.HashSet<string> interjections;
    // all known imitative in the language
    private Gee.HashSet<string> imitatives;
    // all known modal in the language
    private Gee.HashSet<string> modals;

    public POSDeterminer(Gee.HashSet<string> words) {
        createNumeralsHashSet();
        createPronounsHashSet();
        createAuxiliariesHashSet();
        createConjunctionsHashSet();
        createParticlesHashSet();
        createInterjectionsHashSet();
        createImitativesHashSet();
        createModalsHashSet();
        dictionary = new Gee.HashMap<string, Gee.HashSet<int>>();

        foreach (string word in words) {
            dictionary.set(word, new Gee.HashSet<int>());
        }
        foreach (string word in dictionary.keys) {
            parseNoun(word);
            parseAdjective(word);
            parseNumeral(word);
            parsePronoun(word);
            parseVerb(word);
            parseAdverb(word);
            parseAuxiliary(word);
            parseConjunction(word);
            parseParticle(word);
            parseInterjection(word);
            parseImitative(word);
            parseModal(word);
        }
    }

    /**
     * Save words in a files depending on their POS
     */
    public void saveWordsToFiles() {
        Gee.HashMap<int, Gee.HashSet<string>> words = new Gee.HashMap<int, Gee.HashSet<string>>();
        // no foreach for enums yet
        // @see https://bugzilla.gnome.org/show_bug.cgi?id=624691
        words[POS.NOUN] = new Gee.HashSet<string>();
        words[POS.ADJECTIVE] = new Gee.HashSet<string>();
        words[POS.NUMERAL] = new Gee.HashSet<string>();
        words[POS.PRONOUN] = new Gee.HashSet<string>();
        words[POS.VERB] = new Gee.HashSet<string>();
        words[POS.ADVERB] = new Gee.HashSet<string>();
        words[POS.AUXILIARY] = new Gee.HashSet<string>();
        words[POS.CONJUNCTION] = new Gee.HashSet<string>();
        words[POS.PARTICLE] = new Gee.HashSet<string>();
        words[POS.INTERJECTION] = new Gee.HashSet<string>();
        words[POS.IMITATIVE] = new Gee.HashSet<string>();
        words[POS.MODAL] = new Gee.HashSet<string>();
        words[POS.UNKNOWN] = new Gee.HashSet<string>();

        foreach (var entry in dictionary.entries) {
            foreach(int pos in entry.value) {
                words[pos].add(entry.key);
            }
            if (entry.value.size == 0) {
                words[POS.UNKNOWN].add(entry.key);
            }
        }
        string filename;
        foreach (var word in words.entries) {
            switch (word.key) {
                case POS.NOUN:
                    filename = "noun.txt";
                    break;
                case POS.ADJECTIVE:
                    filename = "adjective.txt";
                    break;
                case POS.NUMERAL:
                    filename = "numeral.txt";
                    break;
                case POS.PRONOUN:
                    filename = "pronoun.txt";
                    break;
                case POS.VERB:
                    filename = "verb.txt";
                    break;
                case POS.ADVERB:
                    filename = "adverb.txt";
                    break;
                case POS.AUXILIARY:
                    filename = "auxiliary.txt";
                    break;
                case POS.CONJUNCTION:
                    filename = "conjunction.txt";
                    break;
                case POS.PARTICLE:
                    filename = "particle.txt";
                    break;
                case POS.INTERJECTION:
                    filename = "interjection.txt";
                    break;
                case POS.IMITATIVE:
                    filename = "imitative.txt";
                    break;
                case POS.MODAL:
                    filename = "modal.txt";
                    break;
                default:
                    filename = "unknown.txt";
                    break;
            }

            try {
                var file = File.new_for_path(filename);
                if (file.query_exists()) {
                    file.delete();
                }
                var dos = new DataOutputStream(
                        file.create(FileCreateFlags.REPLACE_DESTINATION));
                foreach (string _word in word.value) {
                    dos.put_string (_word + "\n");
                }
            } catch (Error e) {
                stderr.printf ("%s\n", e.message);
            }
        }
    }

    private void createNumeralsHashSet() {
        numerals = new Gee.HashSet<string>();
        string[] _numerals = {
            "бир", "икки", "уч", "тўрт", "беш", "олти", "етти", "саккиз",
            "тўққиз", "ўн", "йигирма", "ўттиз", "қирқ", "эллик", "олтмиш",
            "етмиш", "саксон", "тўқсон", "юз", "минг", "миллион", "миллиард",
            "триллион"
        };

        foreach (string numeral in _numerals) {
            numerals.add(numeral);
        }
    }

    private void createPronounsHashSet() {
        pronouns = new Gee.HashSet<string>();
        string[] _pronouns = {
            // кишилик
            "мен", "сен", "у", "биз", "сиз", "улар",
            // кўрсатиш
            "бу", "шу", "у", "ўша", "ул", "бул", "шул", "ўшал",
            // сўроқ
            "ким", "нима", "қайси", "қандай", "қанча", "нечанчи", "не", "на",
            "нечун", "нечук", "неча", 
            // белгилаш
            "баъзи", "ҳамма", "барча", "бари", "бутун", "барчаси", "жами",
            // ўзлик
            "ўз", "ўзи",
            // бўлишсизлик: ҳеч ким, ҳеч бири, ...
            // гумон: аллаким, нимадир, ...
        };

        foreach (string pronoun in _pronouns) {
            pronouns.add(pronoun);
        }
    }

    private void createAuxiliariesHashSet() {
        auxiliaries = new Gee.HashSet<string>();
        string[] _auxiliaries = {
            "азбаройи", "билан", "учун", "каби", "илгари", "бурун", "бош",
            "чоғли", "қадар", "сайин", "бўйича", "орқа", "орқали", "ора",
            "сабабли", "кўра", "ён", "ич", "қарши", "ўрта", "томон", "қараб",
            "қарамасдан", "қарамай", "сўнг", "бошқа", "кейин", "ташқари",
            "ост", "уст", "таг", "буён", "бери", "бўлак", "ўзга", "нари",
            "сингари", "янглиғ", "сари", "туфайли", "оша", "бўйлаб", "узра",
            "ичра", "деган", "олд", "чамаси", "ҳақида", "тўғрисида", "ҳолда",
            "йўсинда", "асосан", "қадар", "қараганда", "доир", "биноан",
            "мувофиқ", "каби"
        };

        foreach (string auxiliary in _auxiliaries) {
            auxiliaries.add(auxiliary);
        }
    }

    private void createConjunctionsHashSet() {
        conjunctions = new Gee.HashSet<string>();
        string[] _conjunctions = {
            "ва", "ҳам", "ҳамда", "аммо", "лекин", "бироқ", "ёки", "ёхуд",
            "чунки", "шунинг учун", "токи", "баъзан", "негаки", "балки",
            "араг", "башарти", "гўё", "ё", "гоҳ", "дам", "ҳам", "гоҳо", "хоҳ"
        };

        foreach (string conjunction in _conjunctions) {
            conjunctions.add(conjunction);
        }
    }

    private void createParticlesHashSet() {
        particles = new Gee.HashSet<string>();
        string[] _particles = {
            "фақат", "ҳолбуки", "наҳотки", "ҳатто", "чунки", "ахир", 
            "-им", "-чи", "-ку", "-да", "-у", "-ю", "-а", "-я", "-ми", "-чи",
            "-гина", "-кина", "-қина", "-оқ", "-ёқ"
        };

        foreach (string particle in _particles) {
            particles.add(particle);
        }
    }

    private void createInterjectionsHashSet() {
        interjections = new Gee.HashSet<string>();
        string[] _interjections = {
            "эҳ", "оҳ", "уҳ", "ҳо", "ҳе", "э", "эй", "уф", "туф", "вой",
            "ура", "и-и", "ҳув", "марш", "алло", "салом", "хайр", "офарин",
            "ана", "мана", "куч-куч","баҳ-баҳ", "қурай-қурай", "ҳай", "чу",
            "ту-ту-ту", "кишт-кишт", "так-так", "оббо", "ҳақ", "ҳаҳ", "ҳей",
            "тсс", "бас", "ҳорманг", "раҳмат", "қани", "балли", "баракалла",
            "ажабо", "ай", "айюҳаннос", "ай-ҳай", "алвидо", "иби"
        };

        foreach (string interjection in _interjections) {
            interjections.add(interjection);
        }
    }

    private void createImitativesHashSet() {
        imitatives = new Gee.HashSet<string>();
        string[] _imitatives = {
            "анг-анг", "гур", "тарс", "қарс", "гумбур-гумбур", "ялт-юлт",
            "ярқ", "лип-лип", "шақ", "шиқ", "тарс-турс", "тарақ-туруқ",
            "тақ-туқ", "виш-виш", "пиқ-пиқ", "пақ-пақ", "дук-дук", "пирр",
            "гув-гув", "дув-дув", "шип", "трик-трак", "лим-лим", "биж-биж",
            "лик-лик", "ялт", "ярақ-юруқ", "апил-тапил", "гир-гир", "ғув-ғув",
            "лапанг-лапанг", "пир-пир", "дир-дир", "қалт-қалт", "хиппа",
            "ғиппа", "гуп-гуп", "паққа", "қилтғқилт", "ғовур-ғувур",
            "вақир-вуқур", "тақа-тақ", "паға-паға", "данг", "динг",
            "ҳанг-манг", "ғарч-ғурч", "тап-тап", "ғир-ғир", "қулт-қулт",
            "шалп-шалп"
        };

        foreach (string imitative in _imitatives) {
            imitatives.add(imitative);
        }
    }

    private void createModalsHashSet() {
        modals = new Gee.HashSet<string>();
        string[] _modals = {
            "албатта", "шубҳасиз", "ҳақиқатан", "табиий", "дарҳақиқат",
            "эҳтимол", "балки", "афтидан", "чамаси", "шекилли", "зарур",
            "даркор", "бор", "майли", "хўп", "йўқ", "дуруст", "хай", "хўш",
            "бўпти", "демак", "хуллас", "модомики", "дарвоқе", "айтгандаай",
            "айтмоқчи", "афсус", "эсиз", "аттанг", "ажабмас", "мазмуни"
        };

        foreach (string modal in _modals) {
            modals.add(modal);
        }
    }

    public bool isNoun(string word) {
        return dictionary.has_key(word) &&
            dictionary[word].contains(POS.NOUN);
    }

    public bool isAdjective(string word) {
        return dictionary.has_key(word) &&
            dictionary[word].contains(POS.ADJECTIVE);
    }

    public bool isNumeral(string word) {
        return dictionary.has_key(word) &&
            dictionary[word].contains(POS.NUMERAL);
    }

    public bool isPronoun(string word) {
        return dictionary.has_key(word) &&
            dictionary[word].contains(POS.PRONOUN);
    }

    public bool isVerb(string word) {
        return dictionary.has_key(word) &&
            dictionary[word].contains(POS.VERB);
    }

    public bool isAdverb(string word) {
        return dictionary.has_key(word) &&
            dictionary[word].contains(POS.ADVERB);
    }

    public bool isAuxiliary(string word) {
        return dictionary.has_key(word) &&
            dictionary[word].contains(POS.AUXILIARY);
    }

    public bool isConjunction(string word) {
        return dictionary.has_key(word) &&
            dictionary[word].contains(POS.CONJUNCTION);
    }

    public bool isParticle(string word) {
        return dictionary.has_key(word) &&
            dictionary[word].contains(POS.PARTICLE);
    }

    public bool isInterjection(string word) {
        return dictionary.has_key(word) &&
            dictionary[word].contains(POS.INTERJECTION);
    }

    public bool isImitative(string word) {
        return dictionary.has_key(word) &&
            dictionary[word].contains(POS.IMITATIVE);
    }

    public bool isModal(string word) {
        return dictionary.has_key(word) &&
            dictionary[word].contains(POS.MODAL);
    }

    private void parseWord(string word, string? suffix, string? prefix,
            int word_POS, int? root_POS) {
        if (suffix != null && word.has_suffix(suffix)) {
            // add the word POS to dictionary
            dictionary[word].add(word_POS);

            if (root_POS != null && word.length > suffix.length) {
                string root = word[0:word.length - suffix.length];

                if (root_POS == POS.VERB) {
                    root += "моқ";
                }

                if (dictionary.has_key(root)) {
                    // add the root word POS to dictionary
                    dictionary[root].add(root_POS);
                }
            }
        } else if (prefix != null && word.has_prefix(prefix)) {
            // add the word POS to dictionary
            dictionary[word].add(word_POS);

            if (root_POS != null && word.length > prefix.length) {
                string root = word[prefix.length - 1:word.length];

                if (root_POS == POS.VERB) {
                    root += "моқ";
                }

                if (dictionary.has_key(root)) {
                    // add the root word POS to dictionary
                    dictionary[root].add(root_POS);
                }
            }
        }
    }

    // TODO: once a word has been parsed do not attempt other suffixes or
    // prefixes
    private void parseNoun(string word) {
        // uppercase words can only be nouns
        if (word.get_char(0).isupper()) {
            dictionary[word].add(POS.NOUN);
            return;
        }

        // noun + suffix = noun
        string[] suffixes = {
            // қўшиқчи, боғбон, этикдўз, тошлоқ, одамгарчилик
            "чи", "дош", "каш", "бон", "боз", "паз", "дор",
            "шунос", "соз", "дўз", "нома", "хона",
            "чилик", "гарчилик", "зор", "истон",
            // кичрайтириш ва эркалаш қўшимчалари
            "ча", "чак", "чоқ", "лоқ", "алоқ", "гина", "кина",
            "қина", "ғина", "жон", "хон", "ой", "пошша", "биби",
            "бой", "вой"
        };
        foreach (string suffix in suffixes) {
            parseWord(word, suffix, null, POS.NOUN, POS.NOUN);
        }

        // verb + suffix = noun
        suffixes = {
            // элак, тепки, чидам, ишонч, тозалик, чиқинди
            "ма", "к", "ик", "ук", "қ", "иқ", "уқ", "оқ",
            "ки", "ги", "қи", "ғи", "ғу", "кич", "қич", "ғич",
            "гич", "им", "ум", "инди", "ч", "инч", "лик"
        };
        foreach (string suffix in suffixes) {
            parseWord(word, suffix, null, POS.NOUN, POS.VERB);
        }
    }

    private void parseAdjective(string word) {
        // noun + suffix = adjective
        string[] suffixes = {
            // мевасиз, илмли, эпчил, ёзги, хонаки, илмий, оилавий
            "сиз", "ли", "чил", "ги", "ки", "қи", "ий", "вий",
            // оғзаки, уятчан, тухумсимон, дўппилик, 
            "аки", "чан", "симон", "лик", 
        };
        foreach (string suffix in suffixes) {
            parseWord(word, suffix, null, POS.ADJECTIVE, POS.NOUN);
        }

        suffixes = {
            // кескир, олғир, сезгир
            "қир", "кир", "ғир", "гир",
            // ялтироқ, очиқ, тешик, узуқ, тиришқоқ, тойғоқ
            "қ", "к", "иқ", "уқ", "ук", "қоқ", "ғоқ",
            // куюнчак, тортинчоқ
            "чак", "чоқ", "чиқ",
            // сўлғин, кескин, сотқин, турғун
            "ғин", "ғун", "кин", "қин",
            // билағон, билармон
            "ағон", "мон"
        };
        foreach (string suffix in suffixes) {
            parseWord(word, suffix, null, POS.ADJECTIVE, null);
        }

        string[] prefixes = {
            // бақувват, ноинсоф, боадаб, бетамиз, сергап
            "ба", "бо", "бе", "но", "сер"
        };
        foreach (string prefix in prefixes) {
            parseWord(word, null, prefix, POS.ADJECTIVE, POS.NOUN);
        }
    }

    private void parseNumeral(string word) {
        if (numerals.contains(word)) {
            dictionary[word].add(POS.NUMERAL);
        }
    }

    private void parsePronoun(string word) {
        if (pronouns.contains(word)) {
            dictionary[word].add(POS.PRONOUN);
        }
        // гумон олмошлари
        string[] suffixes = {
            // кимдир, бирор, биров, бирор
            "дир", "оқ", "он", "ор", "ов"
        };
        foreach (string suffix in suffixes) {
            parseWord(word, suffix, null, POS.PRONOUN, null);
        }

        // алла: аллаким, ...
        parseWord(word, null, "алла", POS.PRONOUN, null);
    }

    private void parseVerb(string word) {
        // михламоқ, кўпаймоқ, кечикмоқ
        parseWord(word, "моқ", null, POS.VERB, null);
    }

    private void parseAdverb(string word) {
        string[] suffixes = {
            // тахминан, чексиз, граммлаб, бухороча, йилларча, тириклигича
            // дўстона, бутунлай, сутдай
            "ан", "сиз", "лаб", "ча", "ларча", "лигича",
            "она", "лай", "лайин", "дай", "дек"
        };
        foreach (string suffix in suffixes) {
            parseWord(word, suffix, null, POS.ADVERB, POS.NOUN);
        }
        suffixes = {
            // яширин, кўпинча
            "ин", "инча"
        };
        foreach (string suffix in suffixes) {
            parseWord(word, suffix, null, POS.ADVERB, POS.VERB);
        }
    }

    private void parseAuxiliary(string word) {
        if (auxiliaries.contains(word)) {
            dictionary[word].add(POS.AUXILIARY);
        }
    }

    private void parseConjunction(string word) {
        if (conjunctions.contains(word)) {
            dictionary[word].add(POS.CONJUNCTION);
        }
    }

    private void parseParticle(string word) {
        if (particles.contains(word)) {
            dictionary[word].add(POS.PARTICLE);
        }
    }

    private void parseInterjection(string word) {
        if (interjections.contains(word)) {
            dictionary[word].add(POS.INTERJECTION);
        }
    }

    private void parseImitative(string word) {
        if (imitatives.contains(word)) {
            dictionary[word].add(POS.IMITATIVE);
        }
    }

    private void parseModal(string word) {
        if (modals.contains(word)) {
            dictionary[word].add(POS.MODAL);
        }
    }
}
